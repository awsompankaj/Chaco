const Point = require('../models/point')


const addUserPoint = async(req,res)=>{
    try{
        const phone = req.body.phone
        const userPoint =await Point.find({phone:phone})
        if(userPoint.length === 0){
            const point = new Point({
                name: req.body.name,
                phone: req.body.phone,
                location: req.body.location,
                point: req.body.point,
               
            })
            await point.save()
            return res.status(200).send(point)
            
        }
        res.status(200).send([])
        

    }
    catch(e){
        res.status(400).send(e)
    }
}

const userPointSearch = async(req,res)=>{
    try{
       const phone = req.body.phone
       const userPoint =await Point.find({phone:phone})
        if(userPoint.length === 0){
            return res.status(200).send(userPoint)
        }
        res.status(200).send(userPoint)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const allUserPoint = async(req,res)=>{
    try{
        const usersPoint = await Point.find()
        if(usersPoint.length === 0){
            return res.status(200).send(usersPoint)
        }
        res.status(200).send(usersPoint)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const userEditHistory = async(req,res)=>{
    try{
        const usersPoint = await Point.find({StatusEdit:true})
        if(usersPoint.length === 0){
            return res.status(200).send(usersPoint)
        }
        res.status(200).send(usersPoint)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const editUserPoint = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['point']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{    
        const pointID = req.params.id
        const userPoint = await Point.findById(pointID)
        if(!userPoint){
            return res.status(200).send([])
        }
        // if(req.body.StatusEdit){
        //     userPoint.StatusEdit = req.body.StatusEdit
        // }
        if(req.body.point){

            userPoint.point = (userPoint.point - req.body.point)
            userPoint.StatusEdit = true
        }
        
        await userPoint.save()
        res.status(200).send(userPoint)
    }
    catch(e){
        console.log(e);
        res.status(400).send(e)
    }
}



const deleteUserPoint = async(req,res)=>{
    try{
        const _id = req.params.id
        const userPoint = await Point.findById(_id)
        if(!userPoint){
            return res.status(200).send([])
        }
        await Point.findByIdAndDelete(_id)
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


module.exports = {
    addUserPoint,
    userPointSearch,
    allUserPoint,
    userEditHistory,
    editUserPoint,
    deleteUserPoint,
}