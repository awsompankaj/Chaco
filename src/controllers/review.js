const Review = require('../models/review')
const Menu = require('../models/menu')


const addReview = async(req,res)=>{
    try{
        const mealID = req.params.mealID
        const meal = await Menu.findById(mealID)
        if(!meal){
            return res.status(200).send([])
        }
        const revieww = new Review({
            name: req.body.name,
            desc: req.body.desc,
            rating: req.body.rating,
            mealID: req.params.mealID
        })
        await revieww.save()
        
        const review = await Review.find({mealID:req.params.mealID})
        let AverageReview 
        if(review){
            const values = Object.values(review);
            const sum = values.reduce((accumulator, value) => {
                return accumulator + value.rating;
            }, 0);
            AverageReview = sum / review.length
        }
        const reviewID = revieww.id
        if(meal){
            meal.review = meal.review.concat(reviewID)
            meal.AverageReview = AverageReview
            await meal.save()
        }

        res.status(200).send({revieww,meal})

    }
    catch(e){
        res.status(400).send(e)
    }
}

const allrevie = async(req,res)=>{
    try{
        const _id = req.params.mealID
        const meal = await Menu.findById(_id)
        
        if(!meal){
            return res.status(200).send([])
        }
        const review = await Review.find({mealID:_id})
        if(review.length === 0){
            return res.status(200).send(review)
        }
        res.status(200).send(review)
    }
    catch(e){
        res.status(400).send(e)
    }
}



const deleteRevie = async(req,res)=>{
    try{
        let found
        const _id = req.params.id
        const review = await Review.findById(_id)
        if(!review){
            return res.status(200).send([])
        }
        const mealID = await Menu.find({_id:review.mealID,review:_id})
        const RevieAry = mealID[0].review
        RevieAry.forEach(el => {
            if(el.valueOf() === _id){
                found = el.valueOf()
                // const index = RevieAry.indexOf(el)
                // console.log(index);
                // const PopArray = RevieAry.splice(index, 1)
                // console.log(PopArray);
            }
            
        });
        await Menu.findOneAndUpdate(
            {review:_id} ,
            {
               $pull: {review: found }
             },
             {'new': true}
        )
        await Review.findByIdAndDelete(_id)

        // AverageReview
        const AllReviewWithMealID = await Review.find({mealID:review.mealID})
        let AverageRevieww 
        if(AllReviewWithMealID.length !== 0){
            const values = Object.values(AllReviewWithMealID);
            const sum = values.reduce((accumulator, value) => {
                return accumulator + value.rating;
            }, 0);
            AverageRevieww = sum / AllReviewWithMealID.length
            mealID[0].AverageReview = AverageRevieww
            await mealID[0].save()
            return res.status(200).send("Delete Sucssfly")
        }
        mealID[0].AverageReview = 0
        await mealID[0].save()
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


module.exports = {
    deleteRevie,
    allrevie,
    addReview
}