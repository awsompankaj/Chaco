const Admin = require('../models/admin')

const Addadmin = async(req,res)=>{
    try{
        const admin = new Admin(req.body)
        const token = await admin.generateToken()
        await admin.save()
        res.status(200).send({admin,token})
    }
    catch(e){
        res.status(400).send("Invalid email or password")
    }
}

const adminLogin = async(req,res)=>{                                                                                                                                                          
    try{
        const admin = await Admin.findByCredentials(req.body.email,req.body.password)
        const token = await admin.generateToken()
        res.status(200).send({admin,token})
    }
    catch(e){
        res.status(403).send("Invalid email or password")
    }
}

// const userProfile = async(req,res)=>{
//   try{
//       res.status(200).send(req.user)
//   }
//   catch(e){
//       res.status(404).send(e)
//   }
// }

const adminUpdate = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['name','email','password']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{    
        const id = req.params.id
        const admin = await Admin.findById(id)
        if(!admin){
            return res.status(404).send('No Admin is found')
        }
        updata.forEach((el)=>(admin[el]=req.body[el]))
        await admin.save()
        res.status(200).send(admin)
    }
    catch(e){
        console.log(e);
        res.status(400).send(e)
    }
}

const adminLogout = async(req,res)=>{
    try{
        req.admin.tokens = req.admin.tokens.filter((el)=>{
            return el !== req.token
        })
        await req.admin.save()
        res.status(200).send()

    }
    catch(e){
        res.status(500).send(e)
    }
}

// const adminLogoutAll = async(req,res)=>{
//     try{
//         req.admin.tokens = []
//         await req.admin.save()
//         res.status(200).send('Logout all success')
//     }
//     catch(e){
//         res.status(500).send(e)
//     }
// }

const adminDelete = async(req,res)=>{
    try{
        const id = req.params.id
        const admin = await Admin.findByIdAndDelete(id)
        if(!admin){
            return res.status(404).send("Admin not found")
        }
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(500).send(e)
    }
}

const adminImage = async(req,res)=>{
    try{
        req.admin.image = req.file.buffer
        await req.admin.save()
        res.status(200).send(req.admin)
    }
    catch(e){
        res.status(400).send(e)
    }
}

module.exports = {
    adminImage,
    adminDelete,
    // adminLogoutAll,
    adminLogout,
    adminUpdate,
    adminLogin,
    Addadmin,
    // adminProfile
}