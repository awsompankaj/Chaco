const Menu = require('../models/menu')
const Review = require('../models/review')
const cloud = require('../cloudinary')
const fs = require('fs')

const addMeal = async(req,res)=>{
    try{
        const meal = new Menu({
            name: req.body.name,
            desc: req.body.desc,
            price: req.body.price,
            CategoryName: req.body.CategoryName,
            point: req.body.point,
            image: req.body.image,
            admin: req.admin._id
        })
        await meal.save()
        res.status(200).send(meal)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const getMeal = async(req,res)=>{
    try{
        const _id = req.params.id
        const meal = await Menu.findById(_id).populate('review')
        if(!meal){
            return res.status(200).send([])
        }
        res.status(200).send(meal)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const allMeal = async(req,res)=>{
    try{
        const meal = await Menu.find().populate('review')
        if(meal.length === 0){
            return res.status(200).send(meal)
        }
        res.status(200).send(meal)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const allMealByCategory = async(req,res)=>{
    try{
        categoryName = req.body.CategoryName
        const meal = await Menu.find({CategoryName: categoryName})
        if(meal.length === 0){
            return res.status(200).send(meal)
        }
        res.status(200).send(meal)
    }
    catch(e){
        res.status(400).send(e)
    }
}

const editMeal = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['name','desc','price','point','isAvailable','CategoryName']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{    
        const _id = req.params.id
        const meal = await Menu.findById(_id)
        if(!meal){
            return res.status(200).send([])
        }
        updata.forEach((el)=>(meal[el]=req.body[el]))
        await meal.save()
        res.status(200).send(meal)
    }
    catch(e){
        console.log(e);
        res.status(400).send(e)
    }
}

const imageMealEdit = async(req,res)=>{
    const updata = Object.keys(req.body)
    const alloweUpdatas = ['image']

    let isValid = updata.every((el)=>alloweUpdatas.includes(el))
     if(!isValid){
         return res.status(400).send("Can't updata plz, cheack your edit key")
     }

    try{    
        const _id = req.params.mealid
        const meal = await Menu.findById(_id)
        if(!meal){
            return res.status(200).send([])
        }
        meal.image = req.body.image
        await meal.save()
        res.status(200).send(meal)
    }
    catch(e){
        console.log(e);
        res.status(400).send(e)
    }
}

const deleteMeal = async(req,res)=>{
    try{
        const _id = req.params.id
        const meal = await Menu.findById(_id)
        if(!meal){
            return res.status(200).send([])
        }
        const reviewID = meal.review
        for(const val of reviewID ){
            console.log(val)
            await Review.findByIdAndDelete(val)
        }
        await Menu.findByIdAndDelete(_id)
        res.status(200).send("Delete Sucssfly")
    }
    catch(e){
        res.status(400).send(e)
    }
}


const imageMeal = async(req,res)=>{
    try{
        console.log(req.files[0]);
        const result = await cloud.uploads(req.files[0].path)

        const _id = req.params.id
        const meal = await Menu.findById(_id)
        if(!meal){
            return res.status(200).send([])
        }
        meal.imageUrl = result.url
        await meal.save()
        fs.unlinkSync(req.files[0].path)
        res.status(200).send(meal)
    }
    catch(e){
        res.status(400).send(e)
    }
}

module.exports = {
    addMeal,
    getMeal,
    allMeal,
    allMealByCategory,
    editMeal,
    deleteMeal,
    imageMeal,
    imageMealEdit
}