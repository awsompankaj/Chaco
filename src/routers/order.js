const express = require('express') 
const auth = require('../middelware/auth')
const router = new express.Router()
const orderControllers = require('../controllers/order')


router.post('/addOrder',orderControllers.addOrder)  

router.get('/allOrder',auth,orderControllers.allOrder)

router.post('/SpecificOrder',auth,orderControllers.SpecificOrder)

router.post('/GetStatus',auth,orderControllers.GetStatus)

router.patch('/editOrder/:id',auth,orderControllers.editOrder)

router.delete('/deleteOrder/:id',auth,orderControllers.deleteOrder)



module.exports = router