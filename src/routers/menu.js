const express = require('express') 
const auth = require('../middelware/auth')
const router = new express.Router()
const MenuControllers = require('../controllers/menu')
const uploads = require('../multer')


router.post('/addMeal',auth,MenuControllers.addMeal)  

router.get('/Meal/:id',MenuControllers.getMeal)

router.get('/allMeal',MenuControllers.allMeal)

router.post('/allMealByCategory',MenuControllers.allMealByCategory)

router.patch('/editMeal/:id',auth,MenuControllers.editMeal)

router.delete('/deleteMeal/:id',auth,MenuControllers.deleteMeal)

router.patch('/imageMeal/:mealid',auth,MenuControllers.imageMealEdit)

// router.post('/imageMeal/:id',auth,uploads,MenuControllers.imageMeal)



module.exports = router