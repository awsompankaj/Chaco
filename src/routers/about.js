const express = require('express') 
const auth = require('../middelware/auth')
const router = new express.Router()
const AboutControllers = require('../controllers/about')

router.post('/about',auth,AboutControllers.CreateAbout)  

router.get('/about',AboutControllers.GetAbout)

router.patch('/edit-about',auth,AboutControllers.UpdateAbout)

// router.patch('/edit-Schedule',auth,AboutControllers.UpdateSchedule)

router.patch('/edit-image',auth,AboutControllers.UpdateImage)

router.delete('/remove-about',auth,AboutControllers.RemoveAbout)




module.exports = router