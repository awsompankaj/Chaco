const express = require('express')
const auth = require('../middelware/auth')
const router = new express.Router()
const PointControllers = require('../controllers/point')


router.post('/addUserPoint',PointControllers.addUserPoint)  

router.post('/userPointSearch',PointControllers.userPointSearch)

router.get('/allUserPoint',auth,PointControllers.allUserPoint)

router.get('/userEditHistory',auth,PointControllers.userEditHistory)

router.patch('/editUserPoint/:id',auth,PointControllers.editUserPoint)

router.delete('/deleteUserPoint/:id',auth,PointControllers.deleteUserPoint)




module.exports = router