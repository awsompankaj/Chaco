const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const multer = require('multer')

const adminSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
        trim: true
    },
    email:{
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercasw: true,
        validate(email){
            if(!validator.isEmail(email)){
                throw new Error('Email is invalid')
            }
        }
    },
    password:{
        type: String,
        trim: true,
        required: true,
        minlength: 6
    },
    tokens:[
        {
            type:String,
            required:true
        }
    ],
    image:{
        type: Buffer
    }

})

adminSchema.statics.findByCredentials = async(email,password)=>{
    const admin = await Admin.findOne({email})
    if(!admin){
        throw new Error('No admin found')
    }
    const isMatch = await bcrypt.compare(password,admin.password)
    if(!isMatch){
        throw new Error("Password isn't correct")
    }
    return admin
}


adminSchema.pre('save',async function(next){
    if(this.isModified('password')){
        this.password = await bcrypt.hash(this.password,8)
    }
})

adminSchema.methods.generateToken = async function(){
    const admin = this
    const token = jwt.sign({_id:admin._id.toString()},process.env.JWT_SECRET)
    admin.tokens = admin.tokens.concat(token)
    await admin.save()

    return token
}

adminSchema.methods.toJSON = function(){
    // document
    const admin = this

    // Converts this document into a object
    const adminObject = admin.toObject()

    delete adminObject.password
    delete adminObject.tokens

    return adminObject

}

const Admin = mongoose.model('Admin',adminSchema)

module.exports = Admin