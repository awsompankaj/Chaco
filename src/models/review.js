const mongoose = require('mongoose')

const opts = {
    // Make Mongoose use Unix time (seconds since Jan 1, 1970)
    
    timestamps: { currentTime: () =>new Date().setHours(new Date().getHours() + 2) }
};

const ReviewSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    rating:{
        type: Number,
        default: 5,
        minlength: 0,
        maxlenght: 5
    },
    desc:{
        type: String,
        default:' '
    },
    mealID:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'Menu'
    }

},
opts
)



ReviewSchema.methods.toJSON = function(){
    // document
    const review = this

    // Converts this document into a object
    const reviewObject = review.toObject()

    

    return reviewObject

}

const Review = mongoose.model('Review',ReviewSchema)

module.exports = Review